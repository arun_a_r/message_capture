# Co-operative Capture with message passing

### To Execute

Move to directory containing the source code

```
$ mv message_capture/
$ argos3 -c settings.argos3
```

## Aim

This project aims to capture an object by co-ordination between two robots. In  
order to accomplish this we have taken two types of robots. One of the robots  
roams around looking for objects that must be moved. Once it finds them it calls  
a nearest robot to come and help it in moving that object to the desired location  

## Robotic Controllers

There are two types of robots in this experiment

1. Spotter
2. Helper

Before entering into the finer details how we are going to design the functioning  
controller of the two different robots, we have to understand the scenario or the  
landscape on which both robots are going to work. The below given image shows the  
scenario/landscape.  

![Good](corners.png)

Obstacles are placed randomly in the white area of the landscape. There is also a spotter  
placed in the white area of the landscape. A number of helper robots are placed in the  
black region of the landscape. When all robots and border walls are placed it looks  
something like the below given image.  

![pre_action_image](pre_action_landscape.png)

## Function of Spotter:

The function of the spotter is to find an obstacle and call the nearest robot for  
helper robot for help if it is willing. If the helper robot is willing. Both of  
them together must take them to nearest black area and the spotter must start  
searching for the next obstacle.

So the controller function is coded in lua programming language. To efficiently  
run the controller we try to implement various states like a DFA.

The various states in the spotter are  

1. roam
2. choose
3. approach
4. grab
5. call


### State : 1 roam

In this state the robot tries to avoid all obstacles and the black portion of the  
landscape. A change in state occurs when the robot detects a light source placed  
on one or more obstacles. Once it detects a light source state changes to choose.

### State : 2 choose

In this state we select the closest obstacle to the spotter robot. Once we have  
selected the obstacle we intend to capture we change our orientation toward that  
particular obstacle and then change our state to approach.

### State : 3 approach

We go near the chosen obstacle and as we are sufficiently near it we stop and we  
change the state to grab.

### State : 4 grab

In this state the turret is turned toward the obstacle and we lock the obstacle  
to the robot. Once this is done the state is changed to call.

### State : 5 call

In this state we call any robot around that can come and help us. If more than  
one robot respond we call the nearest one to come to our aid.


## Function of Helper

Helper goes to help the spotter to move the obstacle to the nearest black region  
when called.
