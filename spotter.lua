--------------------------------------------------------------------------------
------------------------------------Spotter-------------------------------------
--------------------------------------------------------------------------------
----------------------------------Global Values---------------------------------
--------------------------------------------------------------------------------
count_time = 0
msg = {}
msg[1] = "Any_one_free"
msg[2] = "Yes_I_am_free"
msg[3] = "ok_come"
msg[4] = "coming"
msg[5] = "beacon"
--------------------------------------------------------------------------------
----------------------------------Init Function---------------------------------
--------------------------------------------------------------------------------
function init()
    self_addr = addr(robot.id)
    log(robot.id," : ",id)
    state = "roam"
    prev_state = "dummy"
    robot.colored_blob_omnidirectional_camera.enable()
    robot.turret.set_position_control_mode()
end
--------------------------------------------------------------------------------
-----------------------------------step fn--------------------------------------
--------------------------------------------------------------------------------
function step()
    --robot.leds.set_single_color(1,"red")
    --robot.leds.set_single_color(12,"red")
    if state ~= prev_state then
        log("Spotter_state = "..state)
    end
    prev_state = state
    if state == "roam" then
        roam()
    elseif state == "choose" then
        choose()
    elseif state == "approach" then
        approach()
    elseif state == "grab" then
        grab()
    elseif state == "call" then
        call()
    elseif state == "specific_call" then
        specific_call()
    elseif state == "waiting_for_final_ack" then
        waiting_for_final_ack()
    elseif state == "waiting" then
        waiting()
    end
end
--------------------------------------------------------------------------------
------------------------------reset & destroy fn--------------------------------
--------------------------------------------------------------------------------
function reset()
end

function destroy()
end
--------------------------------------------------------------------------------
---------------------------------Function addr----------------------------------
------It hashes the robot.id and gives an unique numner between [1,251]---------
--------------------------------------------------------------------------------
function addr(s)
    i = 0
    id = 0
    for c in s:gmatch"." do
        id = id + (string.byte(c) * math.pow(2 , i))
        i = i + 1
    end
    log(id)
    id = math.fmod(id,251) + 1
    return id
end
--------------------------------------------------------------------------------
-------------------------------Roam State---------------------------------------
---------In this state the spotter roams in search of an obstacle---------------
--------------------------------------------------------------------------------
function roam()

    sensingLeft =     robot.proximity[3].value +
                      robot.proximity[4].value +
                      robot.proximity[5].value +
                      robot.proximity[6].value +
                      robot.proximity[2].value +
                      robot.proximity[1].value

    sensingRight =    robot.proximity[19].value +
                      robot.proximity[20].value +
                      robot.proximity[21].value +
                      robot.proximity[22].value +
                      robot.proximity[24].value +
                      robot.proximity[23].value
    if #robot.colored_blob_omnidirectional_camera >= 1 then
        state = "choose"
    elseif sensingLeft ~= 0 then
        robot.wheels.set_velocity(7,3)
    elseif sensingRight ~= 0 then
        robot.wheels.set_velocity(3,7)
    elseif robot.motor_ground[1].value < 0.40 then
        robot.wheels.set_velocity(7,3)
    elseif robot.motor_ground[4].value < 0.40 then
        robot.wheels.set_velocity(3,7)
    else
        robot.wheels.set_velocity(10,10)
    end
end
--------------------------------------------------------------------------------
----------------------------Function choose--------------------------------------
-----This function tries to go to the orient toward the nearest obstacle--------
--------------------------------------------------------------------------------
function choose()
    robot.wheels.set_velocity(0,0)
    dist = robot.colored_blob_omnidirectional_camera[1].distance
    ang =  robot.colored_blob_omnidirectional_camera[1].angle
    for i = 1, #robot.colored_blob_omnidirectional_camera do
        if dist > robot.colored_blob_omnidirectional_camera[i].distance then
            dist = robot.colored_blob_omnidirectional_camera[i].distance
            ang = robot.colored_blob_omnidirectional_camera[i].angle
        end
    end
    if ang > 0.1 then
        robot.wheels.set_velocity(-1,1)
    elseif ang < -0.1 then
        robot.wheels.set_velocity(1,-1)
    elseif ang >= -0.1 and ang <= 0.1 then
        state = "approach"
    end
end
--------------------------------------------------------------------------------
-----------------------------Function approach----------------------------------
------------------------Going near the choosen obstacle-------------------------
--------------------------------------------------------------------------------
function approach()
    x = 0
    for i = 1, 24 do --some modification must be done here as we need not check
                     --all proximity sensors then ones located in fron tshall do
        if x < robot.proximity[i].value then
            x = robot.proximity[i].value
        end
    end
-------trying to keep the orientation while approaching the obstacle------------
    dist = robot.colored_blob_omnidirectional_camera[1].distance
    ang =  robot.colored_blob_omnidirectional_camera[1].angle

    for i = 1, #robot.colored_blob_omnidirectional_camera do

        if dist > robot.colored_blob_omnidirectional_camera[i].distance then
            dist = robot.colored_blob_omnidirectional_camera[i].distance
            ang = robot.colored_blob_omnidirectional_camera[i].angle
        end

    end
    if ang > 0 then
        robot.wheels.set_velocity(5,6)
    end
    if ang < 0 then
        robot.wheels.set_velocity(6,5)
    end
    if ang == 0 then
        robot.wheels.set_velocity(5,5)
    end
-------------trying to slow down when reaching near the obstacle----------------
    if x >= 0.5 then
        robot.wheels.set_velocity((1 - x) * 10, (1 - x) * 10)
    end
    if x >= 0.9 then
        robot.wheels.set_velocity(0,0)
        state = "grab"
    end
end
--------------------------------------------------------------------------------
--------------------------------Function Grab-----------------------------------
------------------------We rotate the turret and grab the box-------------------
--------------------------------------------------------------------------------
function grab()
    grip_ang = 200
    x = robot.proximity[1]
    x.value = 0
    pos = 0
    for i = 1,24 do
        if robot.proximity[i].value >= x.value then
            x = robot.proximity[i]
            pos = i
        end
    end
    if x.value == 1 then
        grip_ang = x.angle
    elseif pos >= 1 and pos <= 12 then
        robot.wheels.set_velocity(0,0.75)
    elseif pos >= 13 and pos <= 24 then
        robot.wheels.set_velocity(0.75,0)
    end
    if grip_ang ~= 200 then
        --log(pos," angle: ",x.angle,grip_ang,"value: ",robot.proximity[pos].value)
        robot.wheels.set_velocity(0,0)
        robot.turret.set_rotation(grip_ang)
        robot.gripper.lock_negative()
        count_time = count_time + 1
    end
    if count_time == 50 then
        robot.gripper.lock_negative()
        robot.turret.set_passive_mode()
        count_time = 0
        state = "call"
    end
end

--------------------------------------------------------------------------------
-----------------------------Function call--------------------------------------
------------The function call is supposed to call the nearest robot-------------
--------------------------------------------------------------------------------
function call()
    broadcast_msg = compose(255,1)
    robot.range_and_bearing.set_data(broadcast_msg)
    log(self_addr," sending ",msg[1], "to all")
    distance = 10000
    nearest = 0
    if #robot.range_and_bearing > 0 then
        for i = 1, #robot.range_and_bearing do
            if robot.range_and_bearing[i].data[2] == self_addr and
                robot.range_and_bearing[i].data[3] == 2 and
                robot.range_and_bearing[i].range < distance then

                distance = robot.range_and_bearing[i].range
                nearest = robot.range_and_bearing[i]
            end
        end
    end
    if nearest ~= 0 then
        state = "specific_call"
    end
end
-------------------------------------------------------------------------------
-------------------------Function specific_call--------------------------------
----------This function calls a specific robot to come-------------------------
-------------------------------------------------------------------------------
function specific_call()
    specific_msg = compose(nearest.data[1],3)
    robot.range_and_bearing.set_data(specific_msg)
    log(self_addr," sending ",msg[3]," to ",nearest.data[1])
    state = "waiting_for_final_ack"
end
--------------------------------------------------------------------------------
---------------------Function waiting_for_final_ack-----------------------------
--------------------------------------------------------------------------------
function waiting_for_final_ack()
    if #robot.range_and_bearing > 0 then
        for i = 1, #robot.range_and_bearing do
            if robot.range_and_bearing[i].data[3] == 4 then
                log(self_addr,"received final ack from",robot.range_and_bearing[i].data[1])
                state = "waiting"
            end
        end
    end
end
--------------------------------------------------------------------------------
-----------------------------function compose-----------------------------------
---This function composes message takes two arguments to address and message----
--------------------------------------------------------------------------------
function compose(to_addr,message)
     comp_msg = {self_addr,to_addr,message}
     return comp_msg
 end
--------------------------------------------------------------------------------
------------------------------Function waiting----------------------------------
--------------------------------------------------------------------------------
function waiting()
    beacon = compose(nearest.data[1],5)
    robot.range_and_bearing.set_data(beacon)
end
