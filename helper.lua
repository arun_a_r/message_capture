--------------------------------------------------------------------------------
--------------------------------------Helper------------------------------------
--------------------------------------------------------------------------------
-----------------------------------Global Variables-----------------------------
--------------------------------------------------------------------------------
l = 0
r = 0
count_time = 0
msg = {}
msg[1] = "Any_one_free"
msg[2] = "Yes_I_am_free"
msg[3] = "ok_come"
msg[4] = "coming"
msg[5] = "beacon"
--------------------------------------------------------------------------------
function init()
    state = "listening"
    prev_state = "dummy"
    self_addr = addr(robot.id)
    log(robot.id," : ",id)
    robot.colored_blob_omnidirectional_camera.enable()
    robot.turret.set_position_control_mode()
end

function step()
    if state ~= prev_state then
        log(robot.id," state = ",state)
    end
    prev_state = state

    if state == "listening" then
        listening()
    elseif state == "responding_to_braodcast" then
        responding_to_braodcast()
    elseif state == "waiting_for_final_call" then
        waiting_for_final_call()
    elseif state == "give_final_ack" then
        give_final_ack()
    elseif state == "orient" then
        orient()
    elseif state == "approach_caller" then
        approach_caller()
    elseif state == "grab_robot" then
        grab_robot()
    elseif state == "pull" then
        robot.wheels.set_velocity(-10,-10)
    end
end
--------------------------------------------------------------------------------
------------------------------reset & destroy fn--------------------------------
--------------------------------------------------------------------------------
function reset()
end
function destroy()
end
--------------------------------------------------------------------------------
---------------------------------Function addr----------------------------------
------It hashes the robot.id and gives an unique numner between [1,251]---------
--------------------------------------------------------------------------------
function addr(s)
    i = 0
    id = 0
    for c in s:gmatch"." do
        id = id + (string.byte(c) * math.pow(2 , i))
        i = i + 1
    end
    log(id)
    id = math.fmod(id,251) + 1
    return id
end
--------------------------------------------------------------------------------
-----------------------------Function listening---------------------------------
--------------------It waits for the broadcast calling it-----------------------
--------------------------------------------------------------------------------
function listening()
    calls = {}
    if #robot.range_and_bearing > 0 then
        for i = 1,#robot.range_and_bearing do
            if robot.range_and_bearing[i].data[2] == 255 then
                table.insert(calls, robot.range_and_bearing[i])
            end
        end
    end
    if #calls > 0 then
        distance = 10000
        caller = 0
        for i = 1, #calls do
            if calls[i].range < distance then
                distance = calls[i].range
                caller = calls[i]
            end
        end
    end
    if #calls > 0 and caller ~= 0 then
        log(self_addr,"received ", msg[caller.data[3]]," from ", caller.data[1])
        state = "responding_to_braodcast"
    end
end
--------------------------------------------------------------------------------
-----------------------Function responding_to_braodcast-------------------------
--------------------------------------------------------------------------------
function responding_to_braodcast()
    --robot.leds.set_all_colors("red")
    send_ack = compose(caller.data[1],2)
    robot.range_and_bearing.set_data(send_ack)
    log(self_addr," sending ",msg[2]," to ",caller.data[1])
    state = "waiting_for_final_call"
end
--------------------------------------------------------------------------------
-----------------------Function waiting_for_final_call--------------------------
--------------------------------------------------------------------------------
function waiting_for_final_call()
    if #robot.range_and_bearing > 0 then
        for i =1, #robot.range_and_bearing do
            if robot.range_and_bearing[i].data[2] == self_addr
                and robot.range_and_bearing[i].data[3] == 3 then

                caller = robot.range_and_bearing[i]
                log (self_addr,"received",msg[3]," from ",robot.range_and_bearing[i].data[1])
                state = "give_final_ack"
            elseif robot.range_and_bearing[i].data[3] == 3
                and robot.range_and_bearing[i].data[2] ~= self_addr then

                state = "listening"
            end
        end
    end
end
--------------------------------------------------------------------------------
-------------------------function give_final_ack--------------------------------
--------------------------------------------------------------------------------
function give_final_ack()
    final_ack = compose(caller.data[1],4)
    robot.range_and_bearing.set_data(final_ack)
    log(self_addr," sending ",msg[4]," to ",caller.data[1])
    state = "orient"
end
--------------------------------------------------------------------------------
-----------------------------function compose-----------------------------------
---This function composes message takes two arguments to address and message----
--------------------------------------------------------------------------------
function compose(to_addr,message)
     comp_msg = {self_addr,to_addr,message}
     return comp_msg
 end
--------------------------------------------------------------------------------
------------------------------function orient-----------------------------------
--------------------------------------------------------------------------------
function orient()
    robot.leds.set_single_color(1,"red")
    robot.leds.set_single_color(12,"red")
    if l ~= 0 and l ~= robot.wheels.left_velocity then
        state = "approach_caller"
    end
    if #robot.range_and_bearing > 0 then
        ang = 200
        for i = 1, #robot.range_and_bearing do
            if robot.range_and_bearing[i].data[2] == self_addr
                and robot.range_and_bearing[i].data[3] == 5 then

                ang = robot.range_and_bearing[i]
            end
        end
        if ang ~= 200 then
            if (ang.horizontal_bearing < 0.05 and ang.horizontal_bearing > -0.05) then
                state = "approach_caller"
            elseif ang.horizontal_bearing > 0 then
                robot.wheels.set_velocity(-0.25,0.25)
            elseif ang.horizontal_bearing < 0 then
                robot.wheels.set_velocity(0.25,-0.25)
            end
        end
    end
    l = robot.wheels.left_velocity
    r = robot.wheels.right_velocity
end
--------------------------------------------------------------------------------
-------------------------function approach_caller-------------------------------
--------------------------------------------------------------------------------
function approach_caller()
    x = 0
    for i = 1,24 do
        if robot.proximity[i].value >= x then
            x = robot.proximity[i].value
        end
    end
    robot.wheels.set_velocity((1 - x) * 10,(1 - x) * 10)
    if x >= 0.95 then
        robot.wheels.set_velocity(0,0)
        state = "grab_robot"
    end
end
--------------------------------------------------------------------------------
--------------------------function grab_robot-----------------------------------
--------------------------------------------------------------------------------
function grab_robot()
    grip_ang = 200
    for i = 1,24 do
        if robot.proximity[i].value >= 0.9 then
            grip_ang = robot.proximity[i].angle
            break
        end
    end
    if grip_ang == 200 then
        robot.wheels.set_velocity(5,5)
    else
        robot.wheels.set_velocity(0,0)
        robot.turret.set_rotation(grip_ang)
        robot.gripper.lock_positive()
        count_time = count_time + 1
    end
    if count_time == 100 then
        robot.gripper.lock_positive()
        robot.turret.set_passive_mode()
        count_time = 0
        state = "pull"
    end
end

--------------------------------------------------------------------------------
---------------------Function orientaion set------------------------------------
--------------------------------------------------------------------------------
function final_orientation()
    x = robot.positioning.position.x
    y = robot.positioning.position.y
    orientaion = {}
    if (x > 0 and y > 0) and (x >= y) then --nearer to top
         orientaion[1] = 0
         orientaion[2] = 1
    end
end
