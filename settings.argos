<?xml version="1.0" ?>
<argos-configuration>

    <!-- ************************* -->
    <!-- * General configuration * -->
    <!-- ************************* -->
    <framework>
        <system threads="0" />
        <experiment length="0"
            ticks_per_second="10"
            random_seed="0" />
    </framework>

    <!-- *************** -->
    <!-- * Controllers * -->
    <!-- *************** -->
    <controllers>
        <lua_controller id="spot_contr">
            <actuators>
                <differential_steering implementation="default" />
                <footbot_turret implementation="default" />
                <footbot_gripper implementation="default" />
                <leds implementation="default" medium="leds" />
                <range_and_bearing implementation="default" />
            </actuators>
            <sensors>
                <positioning implementation="default" />
                <colored_blob_omnidirectional_camera implementation="rot_z_only"
                    medium="leds" show_rays="true" />
                <footbot_motor_ground implementation="rot_z_only" />
                <differential_steering implementation="default" />
                <footbot_proximity implementation="default" show_rays="false" />
                <range_and_bearing implementation="medium" medium="rab" show_rays="true" />
            </sensors>
            <params script="spotter.lua" />
        </lua_controller>

        <lua_controller id="help_contr">
            <actuators>
                <differential_steering implementation="default" />
                <footbot_turret implementation="default" />
                <footbot_gripper implementation="default" />
                <leds implementation="default" medium="leds" />
                <range_and_bearing implementation="default" />
            </actuators>
            <sensors>
                <positioning implementation="default" />
                <colored_blob_omnidirectional_camera implementation="rot_z_only"
                    medium="leds" show_rays="true" />
                <differential_steering implementation="default" />
                <footbot_proximity implementation="default" show_rays="false" />
                <range_and_bearing implementation="medium" medium="rab" show_rays="true" />
            </sensors>
            <params script="helper.lua" />
        </lua_controller>

    </controllers>

    <!-- *********************** -->
    <!-- * Arena configuration * -->
    <!-- *********************** -->
    <arena size="10, 10, 2" center="0, 0, 1">
        <!--Floor design-->
        <floor id="f" source="image" path="corners.png" />

        <!--Adding borders-->
        <box id="bn" size="0.1, 10, 0.2" movable="false">
          <body position="5,0,0"  orientation="0,0,0" />
        </box>
        <box id="bs" size="0.1, 10, 0.2" movable="false">
          <body position="-5,0,0" orientation="0,0,0" />
        </box>
        <box id="be" size="10, 0.1, 0.2" movable="false">
          <body position="0,-5,0" orientation="0,0,0" />
        </box>
        <box id="bw" size="10, 0.1, 0.2" movable="false">
          <body position="0,5,0" orientation="0,0,0" />
        </box>

        <!-- Helper Robots -->
        <!-- RN-fb -->
        <distribute>
          <position method="uniform" min="4,-4.7,0" max="4.7,4.7,0" />
          <orientation method="uniform" min="0,0,0" max="0,0,0" />
          <entity quantity="1" max_trials="100">
            <foot-bot id="RN-fb" rab_range="15" rab_data_size="3" >
              <controller config="help_contr" />
            </foot-bot>
          </entity>
        </distribute>

        <!-- RW-fb -->
        <distribute>
          <position method="uniform" min="-4.7,4,0" max="4.7,4.7,0" />
          <orientation method="uniform" min="0,0,0" max="0,0,0" />
          <entity quantity="1" max_trials="100">
            <foot-bot id="RW-fb" rab_range="15" rab_data_size="3">
              <controller config="help_contr" />
            </foot-bot>
          </entity>
        </distribute>

        <!-- RE-fb -->
        <distribute>
          <position method="uniform" min="-4.7,-4.7,0" max="4.7,-4,0" />
          <orientation method="uniform" min="0,0,0" max="0,0,0" />
          <entity quantity="1" max_trials="100">
            <foot-bot id="RE-fb" rab_range="15" rab_data_size="3">
              <controller config="help_contr" />
            </foot-bot>
          </entity>
        </distribute>

        <!-- RS-fb -->
        <distribute>
          <position method="uniform" min="-4.7,-4.7,0" max="-4,4.7,0" />
          <orientation method="uniform" min="0,0,0" max="0,0,0" />
          <entity quantity="1" max_trials="100">
            <foot-bot id="RS-fb" rab_range="15" rab_data_size="3">
              <controller config="help_contr" />
            </foot-bot>
          </entity>
        </distribute>

        <!-- Spotter Robots -->
        <distribute>
          <position method="uniform" min="-3,-3,0" max="3,3,0" />
          <orientation method="uniform" min="0,0,0" max="0,0,0" />
          <entity quantity="1" max_trials="100">
            <foot-bot id="spotter-fb" rab_range="15" rab_data_size="3">
              <controller config="spot_contr" />
            </foot-bot>
          </entity>
        </distribute>


        <!-- Obstacles -->
        <distribute>
          <position method="uniform" min="-4,-4,0" max="4,4,0" />
          <orientation method="uniform" min="0,0,0" max="0,0,0" />
          <entity quantity="10" max_trials="100">
              <cylinder id="cyl1" radius="0.1" height="0.1"
                            movable="true" mass="2.5">
                    <leds medium="leds">
                    <led offset=" 0,0,0.1" anchor="origin" color="blue" />
                  </leds>
              </cylinder>
          </entity>
        </distribute>


    </arena>

    <!-- ******************* -->
    <!-- * Physics engines * -->
    <!-- ******************* -->
    <physics_engines>
        <dynamics2d id="dyn2d" />
        <pointmass3d id="pm3d" />
    </physics_engines>

    <!-- ********* -->
    <!-- * Media * -->
    <!-- ********* -->
    <media>
        <led id="leds" />
        <range_and_bearing id="rab" />
    </media>

    <!-- ****************** -->
    <!-- * Visualization * -->
    <!-- ****************** -->
    <visualization>
        <qt-opengl lua_editor="true">
            <camera>
                <placement idx="1" position="-5,0,1" look_at="0,0,0" lens_focal_length="20" />
                <placement idx="2" position="5,0,1" look_at="0,0,0" lens_focal_length="20" />
            </camera>
        </qt-opengl>
    </visualization>

</argos-configuration>
